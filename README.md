# libcnbt

A library written in c for *BUILDING* and *SERIALIZING* named-binary-tags (NBTs).

## Word of caution

This library is designed with Minecraft in mind. That is, Minecraft Java Edition as some may call it. Minecraft Bedrock edition is *NOT* supported. This is because I do not care about implementing all the quirks of a blocky Sandbox game that tries to mimic a much better blocky Sandbox game. However, that should not discourage you from implementing it yourself. Pull-requests and forks are always welcome. :)

## TODO

- Error handeling
- Safety checks
- Optional enforcing of Minecraft's NBT-limits (max nested depths)
- Extensive testing
- Automatic compression via GZip

## Resources
[NBT reference](https://minecraft.fandom.com/wiki/NBT_format#Generating_NBT_object)
