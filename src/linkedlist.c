#include "libcnbt.h"

#include <stdlib.h>

struct ll_node *append(struct ll_node *pHead, void *content) {
  struct ll_node *node = malloc(sizeof(struct ll_node));
  node->content = content;

  if (pHead != NULL)
    pHead->pnext = node;

  return node;
}
