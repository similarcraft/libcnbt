#include "libcnbt.h"
#include <string.h>

size_t cnbt_sizeof(enum NBT_TYPE type) {
  size_t bytes = 0;
  switch (type) {
  case NBT_TBYTE: // 1 byte
    bytes = 1;
    break;
  case NBT_TSHORT: // 2 byte
    bytes = 2;
    break;
  case NBT_TINT: // 4 bytes
    bytes = 4;
    break;
  case NBT_TLONG: // 8 bytes
    bytes = 8;
    break;
  case NBT_TFLOAT: // 4 bytes
    bytes = 4;
    break;
  case NBT_TDOUBLE: // 8 bytes
    bytes = 8;
    break;

  default:
    bytes = 0; // undefined
    break;
  }

  return bytes;
}

enum NBT_TYPE cnbt_get_array_type_of_primitive(enum NBT_TYPE primitveType) {
  switch (primitveType) {
  case NBT_TBYTE:
    return NBT_TBYTEARRAY;
  case NBT_TINT:
    return NBT_TINTARRAY;
  case NBT_TLONG:
    return NBT_TLONGARRAY;
  default:
    return -1;
  }
}

enum NBT_TYPE cnbt_get_primitive_type_of_array(enum NBT_TYPE arrayType) {
  switch (arrayType) {
  case NBT_TBYTEARRAY:
  case NBT_TSTRING:
    return NBT_TBYTE;
  case NBT_TINTARRAY:
    return NBT_TINT;
  case NBT_TLONGARRAY:
    return NBT_TLONG;
  default:
    return -1;
  }
}

// Appends a tag to a list
void cnbt_list_append(struct nbt_var *list, void *nbtData) {
  struct nbt_enumerable *listPayload = list->content.pointer;
  struct ll_node *listNode = append(listPayload->contentHead, nbtData);

  // If the child is the first child of the compound
  if (listPayload->contentHead == NULL)
    listPayload->contentHead = listNode;
  listPayload->length++;
}

// Adds a tag to a compound
// NOTE: duplicate names are possible and need to be avoided by the user
void cnbt_compound_add(struct nbt_var *compound, struct nbt_var *child) {
  struct ll_node *listNode = append(compound->content.pointer, child);

  // If the child is the first child of the compound
  if (compound->content.pointer == NULL)
    compound->content.pointer = listNode;
}

// Searches the compound for a tag with the given name
// Returns NULL if none is found
struct nbt_var *cnbt_compound_get(struct nbt_var *compound,
                                  char const *const name) {
  struct ll_node *tagNode = compound->content.pointer;
  while (tagNode != NULL) {
    struct nbt_var *tag = tagNode->content;
    if (strcmp(name, tag->name))
      return tag;
  }
  return NULL;
}
