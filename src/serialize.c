#include "libcnbt.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

// Forward declarations since we do a little indirect recursion
static void serialize_payload(struct nbt_var *v, FILE *f);
static void serialize_tag(struct nbt_var *v, FILE *f);

// Inefficient but convenient method of memcopying integers into a file
// big-endian style
static void fputmem(uint64_t mem, size_t size_in_bytes, FILE *filehandle) {
  for (size_t i = size_in_bytes - 1; i >= 0; i--) {
    fputc((mem >> (i * 8)) % 256, filehandle);
  }
}

// Helper function for serializing nbt-primitives
static void serialize_primitive(struct nbt_var *v, FILE *f) {
// All numbers are stored as big-endian
#ifdef DEBUG
  printf("Serializing tag of type %x, value %ul\n", v->type, v->content.value);
#endif
  fputmem(v->content.value, cnbt_sizeof(v->type), f);
}

// Helper function for serializing nbt-arrays
static void serialize_array(struct nbt_var *v, FILE *f) {
  size_t length = 0;
  switch (v->type) {
    // 4-Byte signed int
  case NBT_TBYTEARRAY:
  case NBT_TINTARRAY:
  case NBT_TLONGARRAY:
    length = 4;
    break;

    // 2-Byte unsigned short
  case NBT_TSTRING:
    length = 2;
    break;
  }

  // Resolve the pointer to the array
  struct nbt_enumerable *array = v->content.pointer;
  fputmem(array->length, length, f);

  // Payload (the array itself)
  size_t unit_size = cnbt_sizeof(array->childType);
  char *array_pointer = NULL;
  for (size_t i = 0; i < length; i++) {
    array_pointer = ((char *)array->contentHead) + i * unit_size;

    // Dereference the array based on the child type so we don't segfault on the
    // first iterations
    switch (array->childType) {
    case NBT_TBYTE:
      fputmem(*((char *)array_pointer), unit_size, f);
      break;
    case NBT_TINT:
      fputmem(*((uint32_t *)array_pointer), unit_size, f);
      break;
    case NBT_TLONG:
      fputmem(*((uint64_t *)array_pointer), unit_size, f);
      break;
    }
  }
}

// Helper function for serializing nbt-lists
static void serialize_list(struct nbt_var *v, FILE *f) {
  //[ChildTAG-ID 1-Byte] [Length 4-Byte signed integer] payload
  struct nbt_enumerable *list = v->content.pointer;

  // ChildTAG-ID
  putc(list->childType, f);

  // Length
  fputmem(list->length, 4, f);

  // Payload
  struct ll_node *entry = list->contentHead;
  for (size_t i = 0; i < list->length; i++) {
    serialize_payload(entry->content, f);
    entry = entry->pnext; // NOTE: This assumes the linked list has as many
                          // nodes as list->length specifies
  }
}

// Helper function for serializing nbt-compounds
static void serialize_compound(struct nbt_var *v, FILE *f) {
  // Payload (List of NBT-Tags)
  struct ll_node *childTagNode = v->content.pointer;
  while (childTagNode != NULL) {
    serialize_tag(childTagNode->content, f);
    childTagNode = childTagNode->pnext;
  }

  // End byte (0x00)
  fputc(0x00, f);
}

// Serializes a component based on its type
static void serialize_tag(struct nbt_var *v, FILE *f) {
  // HEADER
  // -----------
  // [TAGID 1byte][NAMELENGTH 2byte][NAME namelength bytes]

  // TagID 1 byte
  fputc(v->type, f);

  // Name-length 2-byte big-endian unsigned int
  uint16_t namelength = strnlen(v->name, sizeof(uint16_t) * 255);
  fputc(namelength >> 8, f);  // MSByte
  fputc(namelength % 265, f); // LSByte

  // Name n-byte
  for (size_t i = 0; i < namelength; i++)
    fputc(v->name[i], f);

  // PAYLOAD
  // -----------
  serialize_payload(v, f);
}

// Serializing Lists consists of serializing the payload without names or ids
// Since we need this logic twice (Lists and Tags in general) a helper function
// like this simplifys that
static void serialize_payload(struct nbt_var *v, FILE *f) {
  switch (v->type) {
    // Primitives
  case NBT_TBYTE:
  case NBT_TSHORT:
  case NBT_TINT:
  case NBT_TLONG:
  case NBT_TFLOAT:
  case NBT_TDOUBLE:
    serialize_primitive(v, f);
    break;

    // Arrays
  case NBT_TBYTEARRAY:
  case NBT_TINTARRAY:
  case NBT_TLONGARRAY:
  case NBT_TSTRING:
    serialize_array(v, f);
    break;

  // List
  case NBT_TLIST:
    serialize_list(v, f);
    break;

  // Compound
  case NBT_TCOMPOUND:
    serialize_compound(v, f);
    break;
  }
}

void cnbt_serialize_nbt(struct nbt_var *v, char *filename) {
  FILE *f = fopen(filename, "wb");

  serialize_tag(v, f);

  fclose(f);
}
