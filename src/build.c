#include "libcnbt.h"

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

static struct nbt_var *_make_tag(enum NBT_TYPE type, char const *const name) {
  struct nbt_var *tag = malloc(sizeof(struct nbt_var));

  tag->type = type;

  size_t namelength = strlen(name);
  tag->name = malloc(namelength + 1);
  strncpy(tag->name, name, namelength);
  tag->name[namelength] = 0; // 0-terminator

  tag->content.value = 0;

  return tag;
}

static struct nbt_var *_make_primitive(enum NBT_TYPE type, uint64_t value,
                                       char const *const name) {
  struct nbt_var *tag = _make_tag(type, name);
  tag->content.value = value;
  return tag;
}

static struct nbt_var *_make_array(enum NBT_TYPE arrayType, size_t length,
                                   char const *const name) {
  struct nbt_var *tag = _make_tag(arrayType, name);

  struct nbt_enumerable *array = malloc(sizeof(struct nbt_enumerable));
  array->childType = cnbt_get_primitive_type_of_array(arrayType);
  array->length = length;
  array->contentHead = malloc(cnbt_sizeof(array->childType) * length);

  tag->content.pointer = array;

  return tag;
}

struct nbt_var *cnbt_make_byte(uint8_t value, char const *const name) {
  return _make_primitive(NBT_TBYTE, value, name);
}

struct nbt_var *cnbt_make_short(uint16_t value, char const *const name) {
  return _make_primitive(NBT_TSHORT, value, name);
}

struct nbt_var *cnbt_make_integer(uint32_t value, char const *const name) {
  return _make_primitive(NBT_TINT, value, name);
}

struct nbt_var *cnbt_make_long(uint64_t value, char const *const name) {
  return _make_primitive(NBT_TLONG, value, name);
}

struct nbt_var *cnbt_make_float(float value, char const *const name) {
  // Since floats are 32 bits in size (4 bytes) we need to use a nasty pointer
  // trick to avoid a conversion
  uint32_t floatbits = *((uint32_t *)&value);
  return _make_primitive(NBT_TFLOAT, floatbits, name);
}

struct nbt_var *cnbt_make_double(double value, char const *const name) {
  // Same as float, but 8 bytes long
  uint64_t doublebits = *((uint64_t *)&value);
  return _make_primitive(NBT_TDOUBLE, doublebits, name);
}

struct nbt_var *cnbt_make_byteArray(uint8_t const *array, size_t length,
                                    char const *const name) {
  struct nbt_var *tag = _make_array(NBT_TBYTEARRAY, length, name);

  uint8_t *nbtarray =
      ((struct nbt_enumerable *)(tag->content.pointer))->contentHead;
  for (size_t i = 0; i < length; i++)
    nbtarray[i] = array[i];

  return tag;
}

struct nbt_var *cnbt_make_intArray(uint32_t const *array, size_t length,
                                   char const *const name) {
  struct nbt_var *tag = _make_array(NBT_TINTARRAY, length, name);

  uint32_t *nbtarray =
      ((struct nbt_enumerable *)(tag->content.pointer))->contentHead;
  for (size_t i = 0; i < length; i++)
    nbtarray[i] = array[i];

  return tag;
}

struct nbt_var *cnbt_make_longArray(uint64_t const *array, size_t length,
                                    char const *const name) {
  struct nbt_var *tag = _make_array(NBT_TLONGARRAY, length, name);

  uint64_t *nbtarray =
      ((struct nbt_enumerable *)(tag->content.pointer))->contentHead;
  for (size_t i = 0; i < length; i++)
    nbtarray[i] = array[i];

  return tag;
}

struct nbt_var *cnbt_make_string(char const *string, char const *const name) {
  size_t length = strlen(string);
  struct nbt_var *tag = _make_array(NBT_TSTRING, length, name);
  char *nbtarray =
      ((struct nbt_enumerable *)(tag->content.pointer))->contentHead;
  memcpy(nbtarray, string, length);

  return tag;
}

struct nbt_var *cnbt_make_list(enum NBT_TYPE childType,
                               char const *const name) {
  struct nbt_var *tag = _make_tag(NBT_TLIST, name);

  struct nbt_enumerable *list = malloc(sizeof(struct nbt_enumerable));
  list->childType = childType;
  list->contentHead = NULL;
  list->length = 0;

  tag->content.pointer = list;

  return tag;
}

struct nbt_var *cnbt_make_compound(char const *const name) {
  struct nbt_var *tag = _make_tag(NBT_TCOMPOUND, name);
  return tag;
}
