#ifndef LIBCNBT_H_
#define LIBCNBT_H_

#include <stddef.h>
#include <stdint.h>

//
// NBT stuff
//

enum NBT_TYPE {
  NBT_TBYTE = 0x01,
  NBT_TSHORT = 0x02,
  NBT_TINT = 0x03,
  NBT_TLONG = 0x04,
  NBT_TFLOAT = 0x05,
  NBT_TDOUBLE = 0x06,
  NBT_TBYTEARRAY = 0x07, // Heap-allocated char array with len
  NBT_TSTRING = 0x08,
  NBT_TLIST = 0x09,
  NBT_TCOMPOUND =
      0x0A, // Internally represented as linked list due to budget cuts
  NBT_TINTARRAY = 0x0B,
  NBT_TLONGARRAY = 0x0C,
};

// nbt_enumerable serves 2 purpouses:
// LIST: contentHead is the first node of a linked list where content is of the
// type <struct nbt_var>
// ARRAY: contentHead points to a heap-allocated array of type <childType> with
// length <length>
struct nbt_enumerable {
  enum NBT_TYPE childType;
  uint32_t length; // In case of string: strlen(string) - 1
  void *contentHead;
};

// PRIMITIVE: content.value = uint64 representation of value
// COMPOUND: content.pointer = pointer to linked list of nbt_vars
// LIST and ARRAY: content.pointer = pointer to nbt_enumberable
struct nbt_var {
  enum NBT_TYPE type;
  char *name;
  union {
    void *pointer;  // Pointer to nbt_enumerable or <type> array;
    uint64_t value; // double and long primitive -> 8 byte
  } content;
};

// Utilities
// -----------
size_t cnbt_sizeof(enum NBT_TYPE type);
enum NBT_TYPE cnbt_get_array_type_of_primitive(enum NBT_TYPE primitveType);
enum NBT_TYPE cnbt_get_primitive_type_of_array(enum NBT_TYPE arrayType);

// Building
// -----------
// NOTE: All arrays passed to functions will be copied into an array
// Primitives
struct nbt_var *cnbt_make_byte(uint8_t value, char const *const name);
struct nbt_var *cnbt_make_short(uint16_t value, char const *const name);
struct nbt_var *cnbt_make_integer(uint32_t value, char const *const name);
struct nbt_var *cnbt_make_long(uint64_t value, char const *const name);
struct nbt_var *cnbt_make_float(float value, char const *const name);
struct nbt_var *cnbt_make_double(double value, char const *const name);

// Arrays
struct nbt_var *cnbt_make_byteArray(uint8_t const *array, size_t length,
                                    char const *const name);
struct nbt_var *cnbt_make_intArray(uint32_t const *array, size_t length,
                                   char const *const name);
struct nbt_var *cnbt_make_longArray(uint64_t const *array, size_t length,
                                    char const *const name);
struct nbt_var *cnbt_make_string(char const *string, char const *const name);

// Datastructures
struct nbt_var *cnbt_make_list(enum NBT_TYPE childType, char const *const name);
void cnbt_list_append(struct nbt_var *list, void *nbtData);
struct nbt_var *cnbt_make_compound(char const *const name);
void cnbt_compound_add(struct nbt_var *compound, struct nbt_var *child);
struct nbt_var *cnbt_compound_get(struct nbt_var *compound,
                                  char const *const name);

// Serialization
// ---------------
void cnbt_serialize_nbt(struct nbt_var *v, char *filename);

//
// Linked-list stuff
//

struct ll_node {
  struct ll_node *pnext;
  void *content;
};

struct ll_node *append(struct ll_node *pHead, void *content);

#endif // LIBCNBT_H_
