import sys

def usage():
    print("USAGE: ./nbt2snbt.py FILE")
    exit(1)

def main():
    if len(sys.argv) <= 1:
        usage()

    nbtfile = [] # Holds the raw binary of the nbt file
    with open(sys.argv[1], "rb") as f:
        nbtfile = bytearray(f.read())

    if len(nbtfile) == 0:
        print("ERROR reading file " + str(sys.argv[1]))


if __name__ == '__main__':
    main()
